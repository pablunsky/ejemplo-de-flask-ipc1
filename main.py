from flask import Flask, render_template, url_for, request, make_response, session, redirect, jsonify
from Datos.usuario import Usuario
from Datos.receta import Receta
from os import environ
import pdfkit
import json
import base64
import csv
import subprocess
import os

WKHTMLTOPDF_CMD = subprocess.Popen(
    ['which', os.environ.get('WKHTMLTOPDF_BINARY', 'wkhtmltopdf')],
    stdout=subprocess.PIPE).communicate()[0].strip()
config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_CMD)

app = Flask(__name__)

usuario1 = Usuario("admin", "admin")
usuario2 = Usuario("pablito", "123")
usuario3 = Usuario("manolo", "321")

usuarios = [usuario1, usuario2, usuario3]

app.secret_key = b'mimamamemima'

recetas = []


def validar_login(user, contrasena):
    for usuario in usuarios:
        if usuario.nombre == user and usuario.contrasena == contrasena:
            return usuario
    return None


@app.route('/')
def init():
    return redirect('inicio')


@app.route('/inicio')
def home():
    if 'usuario_logeado' in session:
        return render_template('site/inicio.html', usuario=session['usuario_logeado'], recetas=recetas)
    return render_template('site/inicio.html', usuario=None, recetas=recetas)


@app.route('/receta/<int:receta_id>')
def receta(receta_id):
    if receta_id >= len(recetas) or receta_id <= 0:
        return redirect('/inicio')
    receta_seleccionada = recetas[receta_id - 1]
    if 'usuario_logeado' in session:
        return render_template('site/receta.html', usuario=session['usuario_logeado'], receta=receta_seleccionada)
    return render_template('site/receta.html', usuario=session['usuario_logeado'], receta=receta_seleccionada)


@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        usuario = validar_login(
            request.form['usuario'], request.form['contrasena'])
        if usuario != None:
            session['usuario_logeado'] = usuario.nombre
            return redirect('inicio')
        else:
            error = 'Contrasena invalida'
            return render_template('auth/login.html', usuario=None, error=error)
    if 'usuario_logeado' in session:
        return redirect('inicio')
    return render_template('auth/login.html', usuario=None, error=error)


@app.route('/logout', methods=['GET'])
def logout():
    session.pop('usuario_logeado', None)
    return redirect('login')


@app.route('/postReceta', methods=['POST'])
def agregarReceta():
    datos = request.get_json()
    print(datos)
    if datos['titulo'] == '' or datos['contenido'] == '' or datos['imagen'] == '':
        return {"msg": 'Error en contenido'}
    receta = Receta(datos['titulo'], datos['contenido'], datos['imagen'])
    recetas.append(receta)
    return {"msg": 'Receta agregada'}


@app.route('/cargarArchivo', methods=['POST'])
def agregarRecetas():
    datos = request.get_json()

    if datos['data'] == '':
        return {"msg": 'Error en contenido'}

    contenido = base64.b64decode(datos['data']).decode('utf-8')

    filas = contenido.splitlines()
    reader = csv.reader(filas, delimiter=',')
    for row in reader:
        receta = Receta(row[0], row[1], row[2])
        recetas.append(receta)

    return {"msg": 'Receta agregada'}


@app.route('/report', methods=['GET'])
def report():
    html = render_template(
        "reports/recipes.html",
        recetas=recetas)
    pdf = pdfkit.from_string(html, False, configuration=config)
    response = make_response(pdf)
    response.headers["Content-Type"] = "application/pdf"
    response.headers["Content-Disposition"] = "inline; filename=report.pdf"
    return response


if __name__ == '__main__':
    app.run(port=5000)
